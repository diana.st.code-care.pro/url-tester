export enum ManifestStatus {
  Succeed = 'succeed',
  Fail = 'fail',
  SuccessfulFinish = 'finishsuccessful'
}
