import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, interval, timer } from 'rxjs';
import { IConsoleLog } from 'src/app/interfaces/console-log.interface';
import { TerminalService } from 'src/app/services/terminal.service';

@Component({
  selector: 'app-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.scss']
})
export class TerminalComponent implements OnInit {

  public consoleInfo$!: BehaviorSubject<IConsoleLog[]>;

  constructor(
    private terminalService: TerminalService,
  ) { 
    this.consoleInfo$ = this.terminalService.consoleInfo$;
  }

  ngOnInit(): void {
    let terminalContent = document.getElementById('terminal-content');

    this.consoleInfo$.subscribe(result => {
      setTimeout(() => {
        if (terminalContent) {
          terminalContent.scrollTop = terminalContent.scrollHeight;
        }
      }, 0);
    });
  }

}
