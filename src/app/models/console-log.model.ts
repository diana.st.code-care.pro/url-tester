import { ManifestStatus } from "../enums/status.enum";
import { IConsoleLog } from "../interfaces/console-log.interface";

export class ConsoleLog implements IConsoleLog {
	constructor(public message: string, public status?: ManifestStatus) {}
}