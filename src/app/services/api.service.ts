import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {
    
  }

  public callVideoManifest(manifestUrl: string): Observable<string> {
    return this.http.get(manifestUrl, { 
      responseType: 'text'
    });
  }
}
