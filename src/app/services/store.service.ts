import { Injectable } from '@angular/core';
import { TerminalService } from './terminal.service';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  public fileName: string = '';

  private inputData: string[] = [];

  private outputData: string[] = [];

  constructor(private terminalService: TerminalService) { }

  public getInputData(): string[] {
    return this.inputData
  }

  public getOutputData(): string[] {
    return this.outputData
  }

  public getProcessedData(mpd: boolean, m3u8: boolean): string[] {
    let inputDataCopy: string[] = Object.assign(this.inputData);
    let processedData: string[] = [];

    if (mpd && !m3u8) {
      processedData = inputDataCopy;
    } 
    else if(!mpd && m3u8) {
      processedData = inputDataCopy.map(url => url.replace(/mpd/g, "m3u8"));
    } 
    else if(mpd && m3u8) {
      inputDataCopy.forEach(url => {
        processedData.push(url);
        processedData.push(url.replace(/mpd/g, "m3u8"));
      });
    }
    return processedData;
  }

  public updateOutputData(item: string): void {
    this.outputData.push(item)
  }

  public removeOutputData(): void {
    this.outputData = []
  }

  //get file
  private getFile(id: string){
    let control = <HTMLInputElement>document.getElementById(id);
    let files = control.files || [],
        len = files ? files.length : 0;
    // for (let i=0; i < len; i++) {
    //     console.log("Filename: " + files[i].name);
    //     console.log("Type: " + files[i].type);
    //     console.log("Size: " + files[i].size + " bytes");
    // }
    this.fileName = files[0].name
    return files[0];
  }

  //get data from file and return Promise   
  private getDataFile(file: Blob): Promise<string[]>{
    return new Promise((resolve, reject) => {
      this.terminalService.writeHeaderToConsole(`urls start downloading from ${this.fileName}...`);
      let reader = new FileReader();
      reader.readAsText(file);
      if (file.type != 'text/plain'){
        return reject(reader.error?.code);
      }
      reader.onload = () => {
          var contents = reader.result;
          resolve(this.getArrFromString(contents));
      };
      reader.onerror = () => {
          reject(reader.error?.code);
      };
    })
  }

  private getArrFromString(str: any): string[]{
    let re = /\s*[\n,;]\s*/;
    return str.replaceAll(' ','').split(re).filter(Boolean);
  }

  // load urls from file and save
  public getDataFromFile(id: string){
    this.getDataFile(this.getFile(id)).then(
      data => {
        this.inputData = data;
        this.terminalService.writeHeaderToConsole(`${data.length} urils uploaded, click "run" to start testing`);
      },
      error => {
        this.terminalService.writeHeaderToConsole('format not supported select file in .txt format');
      }
    )
  }

  // save the list / string as a file
  public saveStringAsFile(filename: string, type: string) {
    const data: string = this.outputData.join('\n')
    if(data.length === 0){
      this.terminalService.writeHeaderToConsole('No data to save');
      return
    }
    let file = new Blob([data], {type: type});
    let a = document.createElement("a"),
        url = URL.createObjectURL(file);
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    setTimeout(() => {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
      this.terminalService.writeHeaderToConsole('URLs will be saved to txt file');
    }, 0);
  }
}
