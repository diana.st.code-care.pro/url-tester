import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { IConsoleLog } from '../interfaces/console-log.interface';
import { ConsoleLog } from '../models/console-log.model';

@Injectable({
  providedIn: 'root'
})
export class TerminalService {

  public consoleInfo: IConsoleLog[] = [new ConsoleLog('click "Download", select manifest type then click "run" to start testing URLs')];
  public consoleInfo$: BehaviorSubject<IConsoleLog[]> = new BehaviorSubject<IConsoleLog[]>(this.consoleInfo);
  public manifestLogs$: Subject<IConsoleLog> = new Subject<IConsoleLog>();
  public succeedRow: number = 0;

  constructor() { 
    this.subscribeOnManifestInfo();
  }

  private writeToConsole(data: IConsoleLog): void {
    if(this.succeedRow !== 0){
      this.consoleInfo.splice(this.succeedRow, 1);
      this.succeedRow = 0;
    }
    this.consoleInfo.push(data);
    this.consoleInfo$.next(this.consoleInfo)
  }

  private updateConsole(data: IConsoleLog): void {
    if(this.succeedRow === 0){
      this.consoleInfo.push(data);
    }
    else this.consoleInfo.splice(this.succeedRow, 1, data);
    this.consoleInfo$.next(this.consoleInfo);
    this.succeedRow = this.consoleInfo.length - 1;
  }

  private subscribeOnManifestInfo(): void {
    this.manifestLogs$.subscribe((result: IConsoleLog) => {  
      if(result.status === "succeed"){
        this.updateConsole(result);
      }
      else this.writeToConsole(result);
    });
  }

  public writeHeaderToConsole(text: string): void {
    this.writeToConsole(new ConsoleLog('<br>*** ' + text + ' ***'));
  }
}
