import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { ApiService } from './api.service';
import { filter, finalize, take, takeUntil } from 'rxjs/operators';
import { ManifestStatus } from '../enums/status.enum';
import { StoreService } from './store.service';
import { TerminalService } from './terminal.service';
import { UnsubscribeService } from './unsubscribe.service';
import { ConsoleLog } from '../models/console-log.model';

export const CALL_DELAY = 300;
@Injectable({
  providedIn: 'root'
})
export class VideoManifestService {

  public nextVideoTrigger$: Subject<boolean> = new Subject();
  public processRunning$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public processPaused$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public currentVideoManifest: number = 0;
  public videoManifestsUrls: string[] = [];
  public isRejected: boolean = false;

  private callsProcessing: boolean = false;
  private currentManifest: number = 0;
  private timerId: any;
  private isTimeout: boolean = false;

  constructor(
    private apiService: ApiService,
    private terminalService: TerminalService,
    private readonly storeService: StoreService,
    private unsubscribe$: UnsubscribeService,
  ) {
    this.videoManifestSubscriber();
  }

  public setRejected() {
    if(this.isTimeout) {
      this.rejectedLog();
    }
    this.rejectNextCall();
    this.isRejected = true;
    if(this.processPaused$.value) {
      this.rejectedLog();
      this.isRejected = false
    }
  }

  public startCallingVideoManifests(urls: string[], manifestType: string = ''): void {
    this.isRejected = false;
    if(!urls || urls.length === 0){
      this.terminalService.writeHeaderToConsole('not found any urls, click "Download" and then "run"');
      return
    }
    this.processRunning$.next(true);
    this.processPaused$.next(false);

    if (this.currentVideoManifest === 0) {
      this.terminalService.writeHeaderToConsole(`start testing ${urls.length} URLs (${manifestType})...`);
      this.storeService.removeOutputData();
      this.videoManifestsUrls = urls;
    }
    else this.terminalService.writeHeaderToConsole(`continue testing the ${this.videoManifestsUrls.length - this.currentManifest} remaining URLs (${manifestType})...`);

    this.callsProcessing = true;
    this.nextVideoTrigger$.next(true);
  }

  public stopCallingVideoManifests(): void {
    this.processRunning$.next(false);
    this.processPaused$.next(false);

    this.callsProcessing = false;
    this.nextVideoTrigger$.next(false);
    this.currentVideoManifest = 0;
  }

  public pauseCallingVideoManifests(): void {
    if(this.isTimeout) {
      this.terminalService.writeHeaderToConsole('pause testing urls, click "run" to continue or "stop" to finish');
    }
    this.rejectNextCall();
    this.processPaused$.next(true);

    this.callsProcessing = false;
    this.nextVideoTrigger$.next(false);
  }

  private videoManifestSubscriber(): void {
    this.nextVideoTrigger$
    .pipe(
      filter(res => !!res),
      takeUntil(this.unsubscribe$),
    )
    .subscribe(res => {
      const url: string = this.videoManifestsUrls[this.currentVideoManifest];
      this.currentManifest = this.currentVideoManifest + 1;
      this.incrementVideoIndex();

      this.apiService.callVideoManifest(url)
      .pipe(
        take(1),
        finalize(() => { 
          this.triggerNextApiCall(); 
        })
      )
      .subscribe(
        res => {
          this.terminalService.manifestLogs$.next(new ConsoleLog(this.currentManifest + ': ' + url + ' -> ' + 'Succeed', ManifestStatus.Succeed));
        },
        err => {
          this.terminalService.manifestLogs$.next(new ConsoleLog(url + ' -> ' + 'Fail', ManifestStatus.Fail));
          this.storeService.updateOutputData(url);
        }
      )
    });
  }

  private incrementVideoIndex(): void {
    if(this.currentVideoManifest < this.videoManifestsUrls.length - 1) {
      ++this.currentVideoManifest;
    } else {
      this.stopCallingVideoManifests();
    }
  }

  private triggerNextApiCall(): void {
    if(this.currentVideoManifest === 0 && !this.isRejected) {
      this.finishLog();
    }
    if(this.processPaused$.value) {
      this.terminalService.writeHeaderToConsole('pause testing urls, click "run" to continue or "stop" to finish');
    }
    if(this.isRejected) {
      this.rejectedLog();
      this.isRejected = false
    }
    if(this.callsProcessing) {
      this.isTimeout = true;
      this.timerId = setTimeout(() => {
        this.nextVideoTrigger$.next(true);
        this.isTimeout = false;
      }, CALL_DELAY);
      this.isRejected = false
    }
  }

  private rejectNextCall(){
    clearTimeout(this.timerId);
    this.isTimeout = false;
  }

  private rejectedLog(): void{
    const errLength = this.storeService.getOutputData().length;
    this.terminalService.writeHeaderToConsole('testing process is interrupted');
    if(errLength) { 
      this.terminalService.manifestLogs$.next(new ConsoleLog(`${this.currentManifest} from ${this.videoManifestsUrls.length} URLs tested, ${errLength} errors found`));
      this.terminalService.manifestLogs$.next(new ConsoleLog(`click "Save" to save the current results or "run" to start with the same URLs`));
    }
    else {
      this.terminalService.manifestLogs$.next(new ConsoleLog(`${this.currentManifest} from ${this.videoManifestsUrls.length} URLs tested, ${errLength} errors found`));
      this.terminalService.manifestLogs$.next(new ConsoleLog(`click "run" to start again with the same URLs or "Download" to upload new list of URLs`));
    }
  }

  private finishLog(): void{
    const errLength = this.storeService.getOutputData().length;
    this.terminalService.writeHeaderToConsole('FINISH');
    if(errLength) {
      this.terminalService.manifestLogs$.next(new ConsoleLog(`${this.currentManifest} from ${this.videoManifestsUrls.length} URLs tested, ${errLength} errors found, click "Save" to save failed URLs`));
    }
    else {
      this.terminalService.manifestLogs$.next(new ConsoleLog(`${this.currentManifest} from ${this.videoManifestsUrls.length} URLs tested, no errors found`, ManifestStatus.SuccessfulFinish));
    }
  }

}