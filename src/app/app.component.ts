import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { pairwise, startWith } from 'rxjs/operators';
import { StoreService } from './services/store.service';
import { VideoManifestService } from './services/video-manifest.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public manifestType!: FormGroup;

  public processRunning: boolean = false;
  public processPaused: boolean = false;
  
  constructor(
    private readonly storeService: StoreService,
    private videoManifestService: VideoManifestService,
    private fb: FormBuilder,
  ) {
    this.manifestType = this.fb.group({
      mpd: true,
      m3u8: false,
    });

    videoManifestService.processPaused$.subscribe(res => { this.processPaused = res; });
    videoManifestService.processRunning$.subscribe(res => { 
      this.processRunning = res; 
      if(res) {
        this.manifestType.disable();
      } else {
        this.manifestType.enable();
      }
    });
    
    this.manifestType.valueChanges.pipe(startWith(null), pairwise()).subscribe(([prev, next]: [any, any]) => {
      if(!next.mpd && !next.m3u8){
        if(!prev || prev.mpd){
          this.manifestType.patchValue({ m3u8: true })
        }
        else this.manifestType.patchValue({ mpd: true })
      }
    });
  }

  public download(id: string): void{
    this.storeService.getDataFromFile(id)
  }

  public save(){
    this.storeService.saveStringAsFile('not-found-video-manifest.txt', 'text/plain')
  }

  public run(){
    const processedData = this.storeService.getProcessedData(this.manifestType.value.mpd, this.manifestType.value.m3u8);
    this.videoManifestService.startCallingVideoManifests(processedData, this.getManifestType(this.manifestType.value));
  }

  public pause(){
    this.videoManifestService.pauseCallingVideoManifests();
  }

  public stop(){
    this.videoManifestService.setRejected();
    this.videoManifestService.stopCallingVideoManifests();
  }

  private getManifestType(videoManifestType: any): string {
    let manifestTypeStr = '';
    for(let key in videoManifestType){
      if(videoManifestType[key]){
        manifestTypeStr = manifestTypeStr + '.' + key + ' + '
      }
    }
    manifestTypeStr = manifestTypeStr.substring(0, manifestTypeStr.length - 3)
    return manifestTypeStr
  }

}
