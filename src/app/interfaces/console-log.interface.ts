import { ManifestStatus } from "../enums/status.enum";

export interface IConsoleLog {
  message: string;
  status?: ManifestStatus
}